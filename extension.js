const vscode = require('vscode');

function activate(context) {
    const extensionID = 'flectrahq.FlectraSnippets';
    const VscFlectraSnippets = vscode.extensions.getExtension(extensionID);
    const VscFlectraSnippetsVersion = VscFlectraSnippets.packageJSON.version;
    const previousVersion = context.globalState.get('FlectraSnippetsVersion');

    showMessage(VscFlectraSnippetsVersion, previousVersion);

    context.globalState.update('FlectraSnippetsVersion', VscFlectraSnippetsVersion);
}

async function showMessage(version, previousVersion) {
    if (previousVersion === undefined) {
        console.log('VscFlectraSnippets first-time install');
        return;
    }

    if (previousVersion !== version) {
        console.log(`VscFlectraSnippets upgraded from v${previousVersion} to v${version}`);

        const actions = [{ title: "What's New" }, { title: 'Website' }];
        const result = await vscode.window.showInformationMessage(`VscFlectraSnippets has been updated to v${version} — check out what's new!`, ...actions);

        if (result != null) {
            if (result === actions[0]) {
                await vscode.env.openExternal(vscode.Uri.parse('https://gitlab.com/flectra-hq/vscflectrasnippets/blob/master/CHANGELOG.md'));
            } else if (result === actions[1]) {
                await vscode.env.openExternal(vscode.Uri.parse('https://flectrahq.com/dev-tools'));
            }
        }
    }
}

exports.activate = activate;

function deactivate() { }

module.exports = {
    activate,
    deactivate
}
